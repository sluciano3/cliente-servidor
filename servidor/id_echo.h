#include "sys/socket.h" /* Declares our socket functions */
#include "sys/types.h"
#define SERVICE "echo" /* Name of UDP service */
#define BUF_SIZE 1024 /* Maximum size of datagrams that can be read by client and server */
