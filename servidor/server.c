#include <syslog.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#define IS_ADDR_STR_LEN 5
#define CONNECTION_LIMIT 1
#include "id_echo.h"
#include "sys/types.h"
#define SERVICE "echo" 
#define BUF_SIZE 1024

#include<unistd.h> 
#include<string.h> 
#include<pthread.h> 

#include<stdbool.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/sendfile.h>


// funcion que maneja los hilos
void *connection_handler(void*);

void *manejador_peticiones(void*);

void *manejador_espera(void*);

void *manejador_cliente_externo(void*);

void *handler_img(void*);

struct cliente {
	char nombre[50];
	int id_cliente;
	bool activo;
	bool luces_encendidas;
	bool riego_activado;
	bool esperando;
};

//size = cantidad de clientes en el arreglo
int size=10, cont_elem=0, i, pos, opcion=1;
struct cliente cli;
struct cliente clientes[10];

pthread_t thread_id;


// metodo para agregar un cliente a la lista
int agregarCliente(char nombre[], int id_cliente){

	bool encontrado = false;

	for(i = 0; i < size;i++)
	{

		if (strcmp(clientes[i].nombre,nombre)==0)
		{
			clientes[i].id_cliente = id_cliente;
			clientes[i].activo = true;
			clientes[i].esperando = false;
			encontrado = true;
		}
	}

	if(encontrado == true){
		printf("\nCliente ya existente activado \n\n");
		return 1;
	}


    // si el vector lleno, no se puede agregar mas clientes
	if(cont_elem == size){
		printf("No hay mas lugar para añadir clientes\n");
		return -1;
	}else { //no es necesario esto, porque si llego hasta aca quiere decir que esta todo bien, sino salta por alguno de los if anteriores


		for(i = 9; i >=0; i--)
		{
			if (i!=0)
			{
				strcpy(clientes[i].nombre,clientes[i-1].nombre);
				clientes[i].id_cliente=clientes[i-1].id_cliente;
			}
			else
			{
				strcpy(clientes[i].nombre,nombre);
				clientes[i].id_cliente=id_cliente;
			}

			clientes[i].luces_encendidas = false;
			clientes[i].riego_activado = false;
			clientes[i].activo = true;
			clientes[i].esperando = false;
		}

		cont_elem+=1;
		printf("\nCliente añadido. \n");
		printf("cantidad de clientes activos: %d\n\n", cont_elem);
		return 1;
	}

}

// metodo deshabilita un cliente de la lista
void deshabilitarCliente(char nombre[], int id_cliente) {

	int clientes_activos;

	if(cont_elem == 0){
		printf("\n");
		printf("No hay clientes para deshabilitar\n");
		printf("\n\n");
	}else{

		bool encontrado = false;

		for(i = 0; i < size;i++)
		{
			if (strcmp(clientes[i].nombre,nombre)==0)
			{
				clientes[i].activo = false;
				clientes[i].esperando = false;
				encontrado = true;
			}
		}

		if(encontrado == false){
			printf("No se encontro el cliente a deshabilitar");
		}

		printf("cantidad de clientes en la lista: %d\n\n", cont_elem);

	}
}

// metodo usado para realizar una llamada (chat) a otro cliente
int llamada_cliente(char nombre_cliente[], char cliente_a_llamar[]){	
	char msjaviso[80] = "llamada recibida de: ";
	bool encontrado = false; 
	int id_cliente;
	pthread_t thread_id;
	char client_message[2000], eleccion,*message ;
	char *chatkey = "cht\n";
	
	for(i = 0; i < size;i++)
	{

		if (strcmp(clientes[i].nombre,cliente_a_llamar)==0 && clientes[i].activo == true && clientes[i].esperando == true)
		{
			id_cliente = clientes[i].id_cliente;
			encontrado = true;

				//mensaje mostrado en la pantalla del servidor
				//printf("llamada entrante del cliente %s al cliente %s\n",nombre_cliente,cliente_a_llamar);											
				//strcat( msjaviso, nombre_cliente);
				//write(clientes[i].id_cliente , msjaviso , strlen(msjaviso));
				write(clientes[i].id_cliente , chatkey , strlen(chatkey));

		}
	}

	if(encontrado == false){		
		return -1;
	}
	else{

		printf("El cliente atendio la llamada y entro al chat\n\n");
		if( pthread_create( &thread_id , NULL ,  manejador_peticiones , (void*) &id_cliente ) < 0)
		{
			perror("no se pudo crear el hilo");
			return 1;
		}
		
		for(i = 0; i < size;i++)
		{

			if (strcmp(clientes[i].nombre,cliente_a_llamar)==0 && clientes[i].activo == true && clientes[i].esperando == true)
			{
				//pone al cliente en modo espera										
				clientes[i].esperando = false;					
			}
		}
		pthread_join( thread_id , NULL);

	}

	return 1;

}

// metodo usado para enviar una imagen a otro cliente
int enviar_imagen(char nombre_cliente[], char cliente_receptor[]){	
	char msjaviso[80] = "imagen enviada desde cliente: ";
	bool encontrado = false; 
	int id_cliente;
	pthread_t thread_id;
	char client_message[2000], eleccion,*message;
	char *imgkey = "img"; 
	
	for(i = 0; i < size;i++)
	{

		if (strcmp(clientes[i].nombre,cliente_receptor)==0 && clientes[i].activo == true && clientes[i].esperando == true)
		{
			//mensaje mostrado en la pantalla del servidor
			printf("imagen recibida del cliente %s al cliente %s\n",nombre_cliente, cliente_receptor);
			id_cliente = clientes[i].id_cliente;
			encontrado = true;
			strcat( msjaviso, nombre_cliente);
			//write(clientes[i].id_cliente , msjaviso , strlen(msjaviso));
			//write(clientes[i].id_cliente , imgkey , strlen(imgkey));
		}
	}

	if(encontrado == false){
		return -1;
	}
	else{
		//message = "enviando imagen";
		message = "img\n";
		write(id_cliente , message , strlen(message));

		if( pthread_create( &thread_id , NULL ,  handler_img , (void*) &id_cliente ) < 0)
		{
			perror("no se pudo crear el hilo");
		}
      
		puts("Handler asignado");
		for(i = 0; i < size;i++)
		{

			if (strcmp(clientes[i].nombre,cliente_receptor)==0 && clientes[i].activo == true && clientes[i].esperando == true)
			{
				//mensaje mostrado en la pantalla del servidor
				//pone al cliente en modo espera										
				clientes[i].esperando = false;					
			}
		}
		pthread_join( thread_id , NULL);
		
	}

	return 1;
}


////////////////////////////////////////////////////////////////////////////////// Main ///////////////////////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[]){

 	// implementacion TCP concurrente

 	//rellena el arreglo con 'X'
	for (i=0;i<size;i++)
	{
		strcpy(clientes[i].nombre,"X");
		clientes[i].id_cliente=0;
	}

	int socket_desc , client_sock , c;
	struct sockaddr_in server , client;

    //prepara sockaddr_in structure
	inet_aton(argv[1], &(server.sin_addr));
	server.sin_port = htons(atoi(argv[2]));	
	server.sin_family = AF_INET;

    //abrir socket
    //devuelve un descriptor de socket
	socket_desc = socket(AF_INET , SOCK_STREAM , 0);
	if (socket_desc == -1)
	{
		perror("socket failed. Error");
		exit(EXIT_FAILURE);
	}
	puts("Socket creado");

    // invoca a Bind
	if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) == -1)
	{
        //imprime el error del mensaje
		perror("bind failed. Error");
		exit(EXIT_FAILURE);
	}
	puts("Bind hecho");

	int optval = 1;
	if(setsockopt(socket_desc, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &optval, sizeof(optval)) == -1) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

    // listen, el servidor empieza a escuchar las llamadas de los clientes
	if(listen(socket_desc , 5) == -1){
		perror("listen failed. Error");
		exit(EXIT_FAILURE);
	}

    // hilo para manejar cliente externo
	//hilo el cual se ejecuta concurrentemente, no se tiene que hacer un join de este hilo
	if( pthread_create( &thread_id , NULL ,  manejador_cliente_externo , (void*) &client_sock) < 0)
	{
			perror("no se pudo crear el hilo");
			return 1;
	}

    //acepta las conexiones entrantes
	puts("Esperando conexiones entrantes...");
	c = sizeof(struct sockaddr_in);

	while( (client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c)))
	{
		
		if( pthread_create( &thread_id , NULL ,  connection_handler , (void*) &client_sock) < 0)
		{
			perror("no se pudo crear el hilo");
			return 1;
		}
         
		puts("Handler asignado");
	}

	if (client_sock < 0)
	{
		perror("accept failed. Error");
		return 1;
	}

	return 0;
}

/////////////////////////////////////////////////////////////////////////////// Fin main ////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////// Handler cliente externo ///////////////////////////////////////////////////////////////////////////////

void *manejador_cliente_externo(void *socket_desc){

	int sock = *(int*)socket_desc;
	char opcion[BUF_SIZE];
	int read_size;
	
	char eleccion,*message ;
	char nombre_cliente_externo[BUF_SIZE], cliente_receptor[BUF_SIZE];
	int id_cliente=sock;
	int client_sock , c;
	struct sockaddr_in server , client;

	int sfd, bindResult;
	ssize_t numRead;
	socklen_t addrlen, len;
	struct sockaddr_storage claddr;
	char buf[BUF_SIZE];
	char client_message[BUF_SIZE];
	struct sockaddr_in sa;
	int sa_len;

	memset(client_message, 0, BUF_SIZE);
	memset(nombre_cliente_externo, 0, BUF_SIZE);
	memset(opcion, 0, BUF_SIZE);
	memset(cliente_receptor, 0, BUF_SIZE);

	for(;;){

	printf("******************** Portero Electrico ******************** \n");
	printf("Ingrese su nombre de cliente externo\n");

	bzero(nombre_cliente_externo, BUF_SIZE);
    fgets(nombre_cliente_externo, BUF_SIZE, stdin);

	printf("Elegir una opcion del menu:\n\t1.llamar a un cliente\n\t2.enviar imagen a un cliente\n");
	bzero(opcion, BUF_SIZE);
    fgets(opcion, BUF_SIZE, stdin);

	switch(*opcion)
			{
		
				case '1':
				printf("ingrese el nombre del cliente al que quiere llamar\n");
				bzero(cliente_receptor, BUF_SIZE);
    			fgets(cliente_receptor, BUF_SIZE, stdin);
    			
						if (llamada_cliente(nombre_cliente_externo,cliente_receptor) <0){
							printf("El cliente al que intenta llamar no figura en la lista\n");
							
						}						
				break;

				case '2':
				printf("ingrese el nombre del cliente al que quiere enviar una imagen\n");
				bzero(cliente_receptor, BUF_SIZE);
    			fgets(cliente_receptor, BUF_SIZE, stdin);

				if(enviar_imagen(nombre_cliente_externo, cliente_receptor) <0){
					printf("El cliente al que intenta enviar la imagen no figura en la lista\n");
				}				
				break;

				default:
				printf("Error, no selecciono una opcion valida \n");
				break;
			}

			char *exitkey = "exit\n";

			for(i = 0; i < size;i++)
				{

					if (strcmp(clientes[i].nombre,cliente_receptor)==0)
					{

						printf("El cliente externo esta saliendo..\n");
						sleep(3);					
						write(clientes[i].id_cliente , exitkey , strlen(exitkey));
					}
				}

			printf("El cliente externo dejo de usar el portero electrico\n\n");
			}

}


///////////////////////////////////////////////////////////////////////// Handler cliente externo ///////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////// Handler espera ///////////////////////////////////////////////////////////////////////////////////////////

void *manejador_espera(void *socket_desc){
	int sock = *(int*)socket_desc;
	pthread_t thread_id1;
	int read_size;
	char client_message[1024], eleccion,*message ;
	char nombre_cliente[20], cliente_a_llamar[20];
	char *exitkey = "exit\n";
	memset(client_message, 0, BUF_SIZE);

	int client_sock , c;
	struct sockaddr_in server , client;
	c = sizeof(struct sockaddr_in);

	 	recv(sock , client_message , 1024 , 0);

	 	for(;;){

		if(strstr(client_message,exitkey)==0){			
			pthread_exit(NULL);
			break;
		}else{
			recv(sock , client_message , 1024 , 0);
		}		
	}
	pthread_exit(NULL);
}


////////////////////////////////////////////////////////////////////// Handler espera /////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////// Connection Handler /////////////////////////////////////////////////////////////////////////////////////////////////


 // funcion que maneja la conexion para cada hilo
void *connection_handler(void *socket_desc)
{
	pthread_t thread_id1; 
	pthread_t thread_id2; 
	int read_size;
	int sock = *(int*)socket_desc;
	char client_message[2000], eleccion,*message ;
	char nombre_cliente[20], cliente_a_llamar[20];
	int id_cliente=sock;
	char *salir_menu = "Saliendo\n";

	int client_sock , c;
	struct sockaddr_in server , client;
	c = sizeof(struct sockaddr_in);

	void prender_luces(char nombre_cliente[]){

		for(i = 0; i < size;i++)
		{

			if (strcmp(clientes[i].nombre,nombre_cliente)==0)
			{
				if(clientes[i].luces_encendidas == true){
					message = "las luces ya estan encendidas\n";
					write(sock , message , strlen(message));
				}else{
					clientes[i].luces_encendidas = true;
					message = "luces encendidas\n";
					write(sock , message , strlen(message));
					printf("luces encendidas por cliente: %s\n", nombre_cliente);
				}
				
			}
		}
	}

	void apagar_luces(char nombre_cliente[]){

		for(i = 0; i < size;i++)
		{

			if (strcmp(clientes[i].nombre,nombre_cliente)==0)
			{
				if(clientes[i].luces_encendidas == false){
					message = "las luces ya estan apagadas\n";
					write(sock , message , strlen(message));
				}else{
					clientes[i].luces_encendidas = false;
					message = "luces apagadas\n";
					write(sock , message , strlen(message));
					printf("luces apagadas por cliente: %s\n", nombre_cliente);
				}
				
			}
		}
	}	

	void activar_riego(char nombre_cliente[]){

		for(i = 0; i < size;i++)
		{

			if (strcmp(clientes[i].nombre,nombre_cliente)==0)
			{
				if(clientes[i].riego_activado == true){
					message = "el riego automatico ya esta activado\n";
					write(sock , message , strlen(message));
				}else{
					clientes[i].riego_activado = true;
					message = "riego automatico activado\n";
					write(sock , message , strlen(message));
					printf("riego activado por cliente: %s\n", nombre_cliente);
				}
				
			}
		}
	}	

	void desactivar_riego(char nombre_cliente[]){

		for(i = 0; i < size;i++)
		{

			if (strcmp(clientes[i].nombre,nombre_cliente)==0)
			{
				if(clientes[i].riego_activado == false){
					message = "el riego automatico ya esta desactivado\n";
					write(sock , message , strlen(message));
				}else{
					clientes[i].riego_activado = false;
					message = "riego automatico desactivado\n";
					write(sock , message , strlen(message));
					printf("riego desactivado por cliente: %s\n", nombre_cliente);
				}
				
			}
		}
	}

    //envia un mensaje al cliente
	message = "Hola! soy su Connection handler\npor favor, introduzca su nombre de cliente\n";
	write(sock , message , strlen(message));

	read_size = recv(sock , nombre_cliente,20,0);

	if  (read_size < 0){
		printf("error mensaje\n");
	}

	// agrega cliente a la lista
	if (agregarCliente(nombre_cliente, id_cliente) > 0){


		for(;;){

			message = "Elegir una opcion del menu:\n\t1.Entrar en modo espera\n\t2.Entrar a menu de opciones\n";
			write(sock , message , strlen(message));

			read_size = recv(sock , client_message , 2000 , 0);

			switch(*client_message)
			{

				case '1':
				for(i = 0; i < size;i++)
				{

					if (strcmp(clientes[i].nombre,nombre_cliente)==0 && clientes[i].activo == true)
					{						
						//pone al cliente en modo espera										
						clientes[i].esperando = true;								
					}
				}
				message = "Esperando\n";
				write(sock , message , strlen(message));
				if( pthread_create( &thread_id2 , NULL ,  manejador_espera , (void*) &sock ) < 0)
				{
					perror("no se pudo crear el hilo");
				}				
				
				pthread_join( thread_id2 , NULL);			
				break;

				case'2':
				message = "Elegir una opcion del menu:\n\t1.Encender luces\n\t2.Apagar luces\n\t3.Activar riego automatico\n\t4.Desactivar riego automatico\n\t5.Salir\n";
				write(sock , message , strlen(message));

			    //recibe el mensaje del cliente
				while( (read_size = recv(sock , client_message , 2000 , 0)) > 0 )
				{
					client_message[read_size] = '\0';
					switch(*client_message)
					{
						case '1': 
						prender_luces(nombre_cliente);
						break;

						case '2': 
						apagar_luces(nombre_cliente);
						break;

						case '3': 
						activar_riego(nombre_cliente);
						break;

						case '4': 
						desactivar_riego(nombre_cliente);
						break;

						case '5':
						write(sock , salir_menu , strlen(salir_menu));
						printf("Cliente desconectado: %s ",nombre_cliente);
						deshabilitarCliente(nombre_cliente,id_cliente);
						pthread_exit(NULL);

						default:
						message = "Error, no selecciono una opcion valida \n";
						write(sock , message , strlen(message));
						break;
					} // fin switch

					message = "Elegir una opcion del menu:\n\t1.Encender luces\n\t2.Apagar luces\n\t3.Activar riego automatico\n\t4.Desactivar riego automatico\n\t5.Salir\n";
					write(sock , message , strlen(message));

			        //limpia el buffer de mensajes
					memset(client_message, 0, 2000);
				} //fin segundo while

				if(read_size == 0)
				{
					puts("Cliente desconectado");
					deshabilitarCliente(nombre_cliente,id_cliente);
						//fflush(stdout);
				}
				else if(read_size == -1)
				{
					perror("recv failed. Error");
				}
					break; //case 2

					default:
					message = "Error, no selecciono una opcion valida \n";
					write(sock , message , strlen(message));
					break;
				}						
			} // fin for
		}//fin if si el cliente entro con exito
		else{
			message = "No se permiten agregar mas clientes\n";
			write(sock , message , strlen(message));
			pthread_exit(NULL);
		}
	} 

////////////////////////////////////////////////////////////////////// Connection Handler /////////////////////////////////////////////////////////////////////////////////////////////////


void *handler_img(void *socket_desc){

static __thread int id = 0;
static __thread int sock = 0;
id = thread_id;
char bufferimg[BUF_SIZE];
char *filename;
ssize_t n;
char *exitkey = "exit";
int numRead;

char nombre_archivo[BUF_SIZE] = "tux.png";

nombre_archivo[sizeof(nombre_archivo)]='\0';

sock = *((int*) socket_desc);

memset(bufferimg, 0, BUF_SIZE);
strcpy(bufferimg, nombre_archivo);

filename = (char*) malloc(strlen(bufferimg) + 1);
strncpy(filename, bufferimg, strlen(bufferimg) + 1);

off_t filesize;

int fd = open(filename, O_RDONLY);
if (fd < 0) {
	sprintf(bufferimg, "[%d] %s", id, filename);
	perror(bufferimg);
	filesize = htonl(0);
	send(sock, (void*) &filesize, sizeof(filesize), 0);
 
} else {
	struct stat fileinfo;
	fstat(fd, &fileinfo);
	sprintf(bufferimg, "%ld", fileinfo.st_size);
	filesize = htonl(fileinfo.st_size);
	send(sock, (void*) &filesize, sizeof(filesize), 0);
}

int bs, scount = 0;
while ((bs = sendfile(sock, fd, NULL, BUF_SIZE)) > 0) {
	scount += bs;
	printf("[%d] enviando %d bytes ...\n", id, bs);
}

if (bs != -1) {
	printf("[%d] Archivo %s (%d bytes) enviado.\n", id, filename, scount);
} else {
	sprintf(bufferimg, "[%d] sendfile", id);
	perror(bufferimg);
}

free(filename);

close(fd);

pthread_exit(NULL);

}

////////////////////////////////////////////////////////////////////////////// Prueba UDP /////////////////////////////////////////////////////////////////////////////////////

void *manejador_peticiones(void *socket_desc){


int sock = *(int*)socket_desc;

int sfd, bindResult;
ssize_t numRead;
socklen_t addrlen, len;
struct sockaddr_storage claddr;

struct sockaddr_in sa;
int sa_len;

char *exitkey = "exit\n";

sa_len = sizeof(sa);
if(getsockname(sock, (struct sockaddr*) &sa , &sa_len) == -1){
	perror("getsockname fallo ");
}

sa.sin_family = AF_INET;

char buf[BUF_SIZE];
char buf2[BUF_SIZE];
char buf3[BUF_SIZE];
char addrStr[IS_ADDR_STR_LEN];
char mensaje[BUF_SIZE];

memset(buf, 0, BUF_SIZE);
memset(buf2, 0, BUF_SIZE);
memset(buf3, 0, BUF_SIZE);
memset(addrStr, 0, BUF_SIZE);
memset(mensaje, 0, BUF_SIZE);

// abrir socket udp
sfd = socket (AF_INET, SOCK_DGRAM, 0);

if (sfd == -1) {
	perror("socket");
	exit(EXIT_FAILURE);
}

int optval = 1;
if(setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &optval, sizeof(optval)) == -1) {
	perror("setsockopt");
	exit(EXIT_FAILURE);
}

bindResult = bind ( sfd, (struct sockaddr*) &sa, sa_len);

if (bindResult == -1) {
	perror("bind");
	exit(EXIT_FAILURE);
}

// chat udp - simulacion por voz. Recibe y envia datagramas individuales
char temp_buf[2];

for (;;) {


	len = sizeof(struct sockaddr_storage);
	
	//para recibir el tamaño del buffer
	recvfrom(sfd, buf2, BUF_SIZE, 0, (struct sockaddr *) &claddr, &len);
		
	for(i = 0; i < strlen(buf2);i++){
		numRead = recvfrom(sfd, buf, BUF_SIZE, 0, (struct sockaddr *) &claddr, &len);

		//concateno las letras recibidas
		strcat( mensaje, buf);
	}


	printf("Cliente : %s\n", mensaje); 			

	//concatenarlo todo para luego comparar si hay un exit
	if (strcmp(mensaje,exitkey)==0){
		printf("chat terminado, cerrando socket udp..\n");
		close(sfd); 
		pthread_exit(NULL);
	}


	if (numRead == -1){
		printf("error numRead\n");
	}

	memset(mensaje, 0, BUF_SIZE);
	bzero(buf, BUF_SIZE);
	fgets(buf, BUF_SIZE, stdin);

	//para enviar el tamaño del buffer
    strncpy(buf3, buf, strlen(buf));
    sendto(sfd, buf3, sizeof(buf2), MSG_CONFIRM, (const struct sockaddr *) &claddr, len); 

    for(i = 0; i < strlen(buf);i++){

    temp_buf[0] = buf[i];
    temp_buf[1] = '\0';

    sendto(sfd, &temp_buf, sizeof(temp_buf), MSG_CONFIRM, (const struct sockaddr *) &claddr, len); 
    }
    
    //limpia los buffer
    memset(buf, 0, BUF_SIZE);	        
    memset(buf3, 0, BUF_SIZE);
    memset(buf2, 0, BUF_SIZE);

}

} // fin manejador_peticiones


////////////////////////////////////////////////////////////////////// Prueba UDP /////////////////////////////////////////////////////////////////////////////////////////////////////////


