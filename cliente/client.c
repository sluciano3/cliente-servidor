#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h> 
#include <netdb.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <unistd.h>
#include <syslog.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include<string.h> 
#include<pthread.h> 
#include<stdbool.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/sendfile.h>

#define IS_ADDR_STR_LEN 5
#define CONNECTION_LIMIT 1
#define SERVICE "echo" 
#define MAXDATASIZE 1024 

void *handler_chat(void*);

void *handler_img(void*);

int main(int argc, char *argv[])
{
    ////////////////////////////////// implementacion TCP ////////////////////////////////////

    char answer;
    char confirma_llamada;
    pthread_t thread_chat;
    pthread_t thread_img;
    int sockfd, numbytes;  
    char buf[MAXDATASIZE];
    char buf_atender[MAXDATASIZE],buf_afirma[MAXDATASIZE];
    struct sockaddr_in their_addr;

    memset(buf_afirma, 0, MAXDATASIZE);    
    memset(buf, 0, MAXDATASIZE);
    memset(buf_atender, 0, MAXDATASIZE);

    //char *udpkey = "iniciando chat udp";
    char *chatkey = "cht\n";
    //char *imgkey = "enviando imagen"; 
    char *imgkey = "img\n"; 
    char *waitkey = "Esperando\n"; 
    char *afirmar = "y";
    char *negar = "n";
    char *salir_menu = "Saliendo\n";

    //aca no se usa para salir, pero si lo recibe y muestra
    char *exitkey = "exit\n";
    int enter;

    inet_aton(argv[1], &(their_addr.sin_addr)); 
    their_addr.sin_port = htons(atoi(argv[2])); 
    their_addr.sin_family = AF_INET;
    
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }


    if (connect(sockfd, (struct sockaddr *)&their_addr, \
      sizeof(struct sockaddr)) == -1) {
        perror("connect");
    exit(1);
}

bool escuchando = false;

for (;;){

 memset(buf, 0, MAXDATASIZE);
    // imprime el mensaje del servidor
 bzero(buf, MAXDATASIZE);
 numbytes = read(sockfd, buf, MAXDATASIZE);
 if (numbytes < 0) 
  perror("ERROR reading from socket");

if(!strcmp(buf,exitkey)==0){ 
    printf("%s", buf);
}

if(strcmp(buf,salir_menu)==0){ 
    exit(1);
} 

if(strcmp(buf,waitkey)==0){
    escuchando = true;
}

//si no esta en modo espera, el cliente puede ingresar comandos
if(escuchando == false){

  // obtiene el mensaje del cliente
  bzero(buf, MAXDATASIZE);
  fgets(buf, MAXDATASIZE, stdin);

  // envia el mensaje al servidor
  numbytes = write(sockfd, buf, strlen(buf));
  if (numbytes < 0) 
      perror("ERROR writing to socket");

}else{

// sino, si estoy en modo espera
    //if(strstr(buf,"llamada")){
    if(strcmp(buf,chatkey) == 0){

        printf("Atendi la llamada y entro a al chat\n");
        if( pthread_create( &thread_chat , NULL ,  handler_chat , (void*) &sockfd) < 0)
        {
            perror("no se pudo crear el hilo");
            return 1;
        }

        pthread_join( thread_chat , NULL);

        printf("Accion completada, presione una tecla para continuar..\n");
        if( scanf( "%d",&enter) != 0 ) {
            printf("Saliendo del modo espera..\n");
        }  
        escuchando = false;

    }

    //if (strstr(buf,"imagen")){
    if(strcmp(buf,imgkey) == 0 ){

        //entro a handler_img
        printf("recibi la imagen y entro al bucle\n");
        //printf("\n");
        if( pthread_create( &thread_img , NULL ,  handler_img , (void*) &sockfd) < 0)
        {
            perror("no se pudo crear el hilo");
            return 1;
        }
        pthread_join( thread_img , NULL);

        printf("Accion completada, presione una tecla para continuar..\n");

        if( scanf( "%d",&enter) != 0 ) {
            printf("Saliendo del modo espera..\n");
        }  
        escuchando = false;
    }
    
}

}

close(sockfd);

return 0;
}

////////////////////////////////// implementacion TCP ////////////////////////////////////


////////////////////////////////// implementacion UDP ////////////////////////////////////

// chat udp -simulacion comunicacion por voz. Recibe y envia datagramas individuales
void *handler_chat(void *socket_desc){


    struct sockaddr_in sa;
    int sa_len, bindResult, sockfd, n, len, i;
    int sock = *(int*)socket_desc;
    char buf[MAXDATASIZE], buf2[MAXDATASIZE], buf3[MAXDATASIZE], mensaje[MAXDATASIZE];
    char *exitkey = "exit\n";
    char *waitkey = "Esperando\n";
    ssize_t numRead;
    
    sa_len = sizeof(sa);
    if(getpeername(sock, (struct sockaddr*) &sa , &sa_len) == -1){
        perror("getsockname fallo ");
    }
    
    sa.sin_family = AF_INET; 

    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) { 
        perror("socket creation failed"); 
        exit(EXIT_FAILURE); 
    } 

    char temp_buf[2];
    
    for(;;){

        bzero(buf, MAXDATASIZE);
        fgets(buf, MAXDATASIZE, stdin);   
        
        //para enviar el tamaño del buffer
        strncpy(buf2, buf, strlen(buf));
        sendto(sockfd, buf2, sizeof(buf2), MSG_CONFIRM, (const struct sockaddr *) &sa,  
            sizeof(sa)); 

        for(i = 0; i < strlen(buf);i++){

            temp_buf[0] = buf[i];
            temp_buf[1] = '\0';

            sendto(sockfd, &temp_buf, sizeof(temp_buf), MSG_CONFIRM, (const struct sockaddr *) &sa,  
                sizeof(sa)); 
        }

        if (strcmp(buf,exitkey)==0){        
            printf("chat terminado, cerrando socket udp..\n");
            close(sockfd); 
            pthread_exit(NULL);
        }


        len = sizeof(struct sockaddr_storage);

        recvfrom(sockfd, buf3, MAXDATASIZE, MSG_WAITALL, (struct sockaddr *) &sa, &len);

        for(i = 0; i < strlen(buf3);i++){

         if ( numRead =  recvfrom(sockfd, buf, MAXDATASIZE, MSG_WAITALL, (struct sockaddr *) &sa, &len) <0){
            perror("Error en lectura de mensaje");
        }

        strcat( mensaje, buf);
    }


    printf("Server : %s\n", mensaje); 

    memset(buf, 0, MAXDATASIZE);
    memset(buf3, 0, MAXDATASIZE);
    memset(buf2, 0, MAXDATASIZE);            
    memset(mensaje, 0, MAXDATASIZE);

}

}

////////////////////////////////// implementacion UDP ////////////////////////////////////


////////////////////////////////// handler img ///////////////////////////////////////////

void *handler_img(void *socket_desc){

    int sa_len, n, i;
    int sockfd = *(int*)socket_desc;
    char *filename;
    char buffer[MAXDATASIZE];

    memset(buffer, 0, MAXDATASIZE);

    filename = (char*) "tux.png";
    off_t filesize = 0;
    int br;

    recv(sockfd, &filesize, sizeof(filesize), 0);
    filesize = ntohl(filesize);
    if (filesize == 0) {
        printf("El archivo no se encontro en el servidor.\n");
    } else {
        printf("Archivo %s encontrado (%ld bytes).\n", filename, filesize);
    }


    int fd;
    int flags = S_IWUSR | S_IRUSR | S_IRGRP | S_IROTH;
    fd = open(filename, O_CREAT | O_TRUNC | O_WRONLY, flags);
    if (fd < 0) {
        perror("open");
    }

    int bcount = 0;
    while ((br = recv(sockfd, buffer, MAXDATASIZE, 0)) > 0) {
        bcount += br;
        printf("Recibiendo %d bytes...\n", br);
        write(fd, buffer, br);
        if (bcount == filesize) {
            break;
        }
    }

    if (br == -1) {
        perror("recv");
    }

    printf("Archivo recibido: %s (%d bytes).\n", filename, bcount);

    close(fd);

    pthread_exit(NULL);

}